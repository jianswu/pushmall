package co.pushmall.mp.service;


import co.pushmall.mp.domain.PushMallWechatMenu;
import co.pushmall.mp.service.dto.PushMallWechatMenuDTO;
import co.pushmall.mp.service.dto.PushMallWechatMenuQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-06
 */
//@CacheConfig(cacheNames = "PushMallWechatMenu")
public interface PushMallWechatMenuService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallWechatMenuQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallWechatMenuDTO> queryAll(PushMallWechatMenuQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param key
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallWechatMenuDTO findById(String key);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallWechatMenuDTO create(PushMallWechatMenu resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallWechatMenu resources);

    /**
     * 删除
     *
     * @param key
     */
    //@CacheEvict(allEntries = true)
    void delete(String key);

    boolean isExist(String key);
}
