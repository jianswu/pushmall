package co.pushmall.mp.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.mp.domain.PushMallWechatReply;
import co.pushmall.mp.service.dto.PushMallWechatReplyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallWechatReplyMapper extends EntityMapper<PushMallWechatReplyDTO, PushMallWechatReply> {

}
