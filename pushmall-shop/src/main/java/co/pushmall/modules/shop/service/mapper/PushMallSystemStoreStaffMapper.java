package co.pushmall.modules.shop.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.shop.domain.PushMallSystemStoreStaff;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2020-03-22
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallSystemStoreStaffMapper extends BaseMapper<PushMallSystemStoreStaffDto, PushMallSystemStoreStaff> {

}
