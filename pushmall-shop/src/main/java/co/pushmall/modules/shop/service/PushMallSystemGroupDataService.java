package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemGroupData;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-18
 */
//@CacheConfig(cacheNames = "yxSystemGroupData")
public interface PushMallSystemGroupDataService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallSystemGroupDataQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallSystemGroupDataDTO> queryAll(PushMallSystemGroupDataQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallSystemGroupDataDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallSystemGroupDataDTO create(PushMallSystemGroupData resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallSystemGroupData resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
