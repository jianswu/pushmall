package co.pushmall.modules.shop.rest;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.constant.ShopConstants;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.shop.domain.PushMallSystemGroupData;
import co.pushmall.modules.shop.service.PushMallSystemGroupDataService;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataQueryCriteria;
import co.pushmall.utils.OrderUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-10-18
 */
@Api(tags = "商城:数据配置管理")
@RestController
@RequestMapping("api")
public class SystemGroupDataController {

    private final PushMallSystemGroupDataService pushMallSystemGroupDataService;

    public SystemGroupDataController(PushMallSystemGroupDataService pushMallSystemGroupDataService) {
        this.pushMallSystemGroupDataService = pushMallSystemGroupDataService;
    }

    @Log("查询数据配置")
    @ApiOperation(value = "查询数据配置")
    @GetMapping(value = "/PmSystemGroupData")
    @PreAuthorize("@el.check('admin','YXSYSTEMGROUPDATA_ALL','YXSYSTEMGROUPDATA_SELECT')")
    public ResponseEntity getPushMallSystemGroupDatas(PushMallSystemGroupDataQueryCriteria criteria,
                                                      Pageable pageable) {
        Sort sort = new Sort(Sort.Direction.DESC, "sort");
        Pageable pageableT = PageRequest.of(pageable.getPageNumber(),
                pageable.getPageSize(),
                sort);
        return new ResponseEntity(pushMallSystemGroupDataService.queryAll(criteria, pageableT), HttpStatus.OK);
    }

    @Log("新增数据配置")
    @ApiOperation(value = "新增数据配置")
    @PostMapping(value = "/PmSystemGroupData")
    @CacheEvict(cacheNames = ShopConstants.PUSHMALL_REDIS_INDEX_KEY, allEntries = true)
    @PreAuthorize("@el.check('admin','YXSYSTEMGROUPDATA_ALL','YXSYSTEMGROUPDATA_CREATE')")
    public ResponseEntity create(@RequestBody String jsonStr) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        JSONObject jsonObject = JSON.parseObject(jsonStr);

        if (ObjectUtil.isNotNull(jsonObject.get("name"))) {
            if (StrUtil.isEmpty(jsonObject.get("name").toString())) {
                throw new BadRequestException("名称必须填写");
            }
        }

        if (ObjectUtil.isNotNull(jsonObject.get("title"))) {
            if (StrUtil.isEmpty(jsonObject.get("title").toString())) {
                throw new BadRequestException("标题必须填写");
            }
        }

        if (ObjectUtil.isNotNull(jsonObject.get("info"))) {
            if (StrUtil.isEmpty(jsonObject.get("info").toString())) {
                throw new BadRequestException("简介必须填写");
            }
        }

        if (ObjectUtil.isNotNull(jsonObject.get("pic"))) {
            if (StrUtil.isEmpty(jsonObject.get("pic").toString())) {
                throw new BadRequestException("图片必须上传");
            }
        }


        PushMallSystemGroupData pushMallSystemGroupData = new PushMallSystemGroupData();
        pushMallSystemGroupData.setGroupName(jsonObject.get("groupName").toString());
        jsonObject.remove("groupName");
        pushMallSystemGroupData.setValue(jsonObject.toJSONString());
        pushMallSystemGroupData.setStatus(jsonObject.getInteger("status"));
        pushMallSystemGroupData.setSort(jsonObject.getInteger("sort"));
        pushMallSystemGroupData.setAddTime(OrderUtil.getSecondTimestampTwo());

        return new ResponseEntity(pushMallSystemGroupDataService.create(pushMallSystemGroupData), HttpStatus.CREATED);
    }

    @Log("修改数据配置")
    @ApiOperation(value = "修改数据配置")
    @PutMapping(value = "/PmSystemGroupData")
    @CacheEvict(cacheNames = ShopConstants.PUSHMALL_REDIS_INDEX_KEY, allEntries = true)
    @PreAuthorize("@el.check('admin','YXSYSTEMGROUPDATA_ALL','YXSYSTEMGROUPDATA_EDIT')")
    public ResponseEntity update(@RequestBody String jsonStr) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        if (ObjectUtil.isNotNull(jsonObject.get("name"))) {
            if (StrUtil.isEmpty(jsonObject.get("name").toString())) {
                throw new BadRequestException("名称必须填写");
            }
        }

        if (ObjectUtil.isNotNull(jsonObject.get("title"))) {
            if (StrUtil.isEmpty(jsonObject.get("title").toString())) {
                throw new BadRequestException("标题必须填写");
            }
        }

        if (ObjectUtil.isNotNull(jsonObject.get("pic"))) {
            if (StrUtil.isEmpty(jsonObject.get("pic").toString())) {
                throw new BadRequestException("图片必须上传");
            }
        }

        PushMallSystemGroupData pushMallSystemGroupData = new PushMallSystemGroupData();

        pushMallSystemGroupData.setGroupName(jsonObject.get("groupName").toString());
        jsonObject.remove("groupName");
        pushMallSystemGroupData.setValue(jsonObject.toJSONString());
        if (jsonObject.getInteger("status") == null) {
            pushMallSystemGroupData.setStatus(1);
        } else {
            pushMallSystemGroupData.setStatus(jsonObject.getInteger("status"));
        }

        if (jsonObject.getInteger("sort") == null) {
            pushMallSystemGroupData.setSort(0);
        } else {
            pushMallSystemGroupData.setSort(jsonObject.getInteger("sort"));
        }


        pushMallSystemGroupData.setId(Integer.valueOf(jsonObject.get("id").toString()));
        pushMallSystemGroupDataService.update(pushMallSystemGroupData);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除数据配置")
    @ApiOperation(value = "删除数据配置")
    @DeleteMapping(value = "/PmSystemGroupData/{id}")
    @PreAuthorize("@el.check('admin','YXSYSTEMGROUPDATA_ALL','YXSYSTEMGROUPDATA_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallSystemGroupDataService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
