package co.pushmall.modules.shop.service.dto;

import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-02
 */
@Data
public class PushMallStoreOrderStatusQueryCriteria {
}
