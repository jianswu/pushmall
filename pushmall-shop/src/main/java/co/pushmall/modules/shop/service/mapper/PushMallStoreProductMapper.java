package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallStoreProduct;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


/**
 * @author pushmall
 * @date 2019-10-04
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreProductMapper extends EntityMapper<PushMallStoreProductDTO, PushMallStoreProduct> {

}
