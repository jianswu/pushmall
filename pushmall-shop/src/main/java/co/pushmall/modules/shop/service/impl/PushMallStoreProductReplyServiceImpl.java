package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallStoreProductReply;
import co.pushmall.modules.shop.repository.PushMallStoreProductReplyRepository;
import co.pushmall.modules.shop.service.PushMallStoreProductReplyService;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallStoreProductReplyMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-03
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreProductReplyServiceImpl implements PushMallStoreProductReplyService {

    private final PushMallStoreProductReplyRepository pushMallStoreProductReplyRepository;

    private final PushMallStoreProductReplyMapper pushMallStoreProductReplyMapper;


    public PushMallStoreProductReplyServiceImpl(PushMallStoreProductReplyRepository pushMallStoreProductReplyRepository,
                                                PushMallStoreProductReplyMapper pushMallStoreProductReplyMapper) {
        this.pushMallStoreProductReplyRepository = pushMallStoreProductReplyRepository;
        this.pushMallStoreProductReplyMapper = pushMallStoreProductReplyMapper;

    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreProductReplyQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreProductReply> page = pushMallStoreProductReplyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreProductReplyMapper::toDto));
    }

    @Override
    public List<PushMallStoreProductReplyDTO> queryAll(PushMallStoreProductReplyQueryCriteria criteria) {
        return pushMallStoreProductReplyMapper.toDto(pushMallStoreProductReplyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreProductReplyDTO findById(Integer id) {
        Optional<PushMallStoreProductReply> yxStoreProductReply = pushMallStoreProductReplyRepository.findById(id);
        ValidationUtil.isNull(yxStoreProductReply, "PushMallStoreProductReply", "id", id);
        return pushMallStoreProductReplyMapper.toDto(yxStoreProductReply.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreProductReplyDTO create(PushMallStoreProductReply resources) {
        return pushMallStoreProductReplyMapper.toDto(pushMallStoreProductReplyRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreProductReply resources) {
        Optional<PushMallStoreProductReply> optionalPushMallStoreProductReply = pushMallStoreProductReplyRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreProductReply, "PushMallStoreProductReply", "id", resources.getId());
        PushMallStoreProductReply pushMallStoreProductReply = optionalPushMallStoreProductReply.get();
        pushMallStoreProductReply.copy(resources);
        pushMallStoreProductReplyRepository.save(pushMallStoreProductReply);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreProductReplyRepository.deleteById(id);
    }
}
