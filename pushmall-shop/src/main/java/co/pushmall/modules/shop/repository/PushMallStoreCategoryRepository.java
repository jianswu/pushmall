package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author pushmall
 * @date 2019-10-03
 */
public interface PushMallStoreCategoryRepository extends JpaRepository<PushMallStoreCategory, Integer>, JpaSpecificationExecutor {
    @Query(value = "select cate_name from pushmall_store_category where id = ?1", nativeQuery = true)
    String findNameById(Integer id);

    @Modifying
    @Query("update PushMallStoreCategory s set s.isDel = 1 where s.id =:id")
    void delCategory(Integer id);

    PushMallStoreCategory findByPid(Integer pid);

    @Query(value = "select id,pid,cate_name,sort,pic,is_show,add_time,is_del from pushmall_store_category where cate_name = :name and is_del = 0 ORDER BY add_time DESC LIMIT 1 ", nativeQuery = true)
    PushMallStoreCategory findByName(String name);

    @Query(value = "select id,pid,cate_name,sort,pic,is_show,add_time,is_del from pushmall_store_category where id = getGrpTopId(:id) and is_del = 0 ", nativeQuery = true)
    PushMallStoreCategory findParentById(Integer id);

    @Query(value = "select id,pid,cate_name,sort,pic,is_show,add_time,is_del from pushmall_store_category where FIND_IN_SET(id,getGrpParIds(:id)) and is_del = 0 ", nativeQuery = true)
    List<PushMallStoreCategory> findParentsById(Integer id);
}
