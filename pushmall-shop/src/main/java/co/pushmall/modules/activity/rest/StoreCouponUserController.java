package co.pushmall.modules.activity.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.service.PushMallStoreCouponUserService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-10
 */
@Api(tags = "商城:优惠券发放记录管理")
@RestController
@RequestMapping("api")
public class StoreCouponUserController {

    private final PushMallStoreCouponUserService pushMallStoreCouponUserService;

    public StoreCouponUserController(PushMallStoreCouponUserService pushMallStoreCouponUserService) {
        this.pushMallStoreCouponUserService = pushMallStoreCouponUserService;
    }

    @Log("查询Y")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmStoreCouponUser")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONUSER_ALL','YXSTORECOUPONUSER_SELECT')")
    public ResponseEntity getPushMallStoreCouponUsers(PushMallStoreCouponUserQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreCouponUserService.queryAll(criteria, pageable), HttpStatus.OK);
    }


}
