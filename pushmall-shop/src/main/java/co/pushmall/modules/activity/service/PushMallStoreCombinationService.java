package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreCombination;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-18
 */
//@CacheConfig(cacheNames = "yxStoreCombination")
public interface PushMallStoreCombinationService {

    void onSale(Integer id, Integer status);

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCombinationQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCombinationDTO> queryAll(PushMallStoreCombinationQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCombinationDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCombinationDTO create(PushMallStoreCombination resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCombination resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
