package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreVisit;
import co.pushmall.modules.activity.service.dto.PushMallStoreVisitDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreVisitQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-18
 */
//@CacheConfig(cacheNames = "yxStoreVisit")
public interface PushMallStoreVisitService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreVisitQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreVisitDTO> queryAll(PushMallStoreVisitQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreVisitDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreVisitDTO create(PushMallStoreVisit resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreVisit resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
