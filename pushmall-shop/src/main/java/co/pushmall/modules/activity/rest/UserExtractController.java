package co.pushmall.modules.activity.rest;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.activity.domain.PushMallUserExtract;
import co.pushmall.modules.activity.service.PushMallUserExtractService;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractQueryCriteria;
import co.pushmall.modules.shop.domain.PushMallUserBill;
import co.pushmall.modules.shop.service.PushMallUserBillService;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.modules.shop.service.PushMallWechatUserService;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import co.pushmall.modules.shop.service.dto.PushMallWechatUserDTO;
import co.pushmall.mp.service.PushMallPayService;
import co.pushmall.utils.OrderUtil;
import com.github.binarywang.wxpay.exception.WxPayException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @author pushmall
 * @date 2019-11-14
 */
@Api(tags = "商城:提现管理")
@RestController
@RequestMapping("api")
public class UserExtractController {

    private final PushMallUserExtractService pushMallUserExtractService;
    private final PushMallUserService pushMallUserService;
    private final PushMallUserBillService pushMallUserBillService;
    private final PushMallWechatUserService wechatUserService;
    private final PushMallPayService payService;

    public UserExtractController(PushMallUserExtractService pushMallUserExtractService, PushMallUserService pushMallUserService,
                                 PushMallUserBillService pushMallUserBillService, PushMallWechatUserService wechatUserService,
                                 PushMallPayService payService) {
        this.pushMallUserExtractService = pushMallUserExtractService;
        this.pushMallUserService = pushMallUserService;
        this.pushMallUserBillService = pushMallUserBillService;
        this.wechatUserService = wechatUserService;
        this.payService = payService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmUserExtract")
    @PreAuthorize("@el.check('admin','YXUSEREXTRACT_ALL','YXUSEREXTRACT_SELECT')")
    public ResponseEntity getPushMallUserExtracts(PushMallUserExtractQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallUserExtractService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @Log("修改")
    @ApiOperation(value = "修改审核")
    @PutMapping(value = "/PmUserExtract")
    @PreAuthorize("@el.check('admin','YXUSEREXTRACT_ALL','YXUSEREXTRACT_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallUserExtract resources) {
        if (StrUtil.isEmpty(resources.getStatus().toString())) {
            throw new BadRequestException("请选择审核状态");
        }
        if (resources.getStatus() != -1 && resources.getStatus() != 1) {
            throw new BadRequestException("请选择审核状态");
        }
        if (resources.getStatus() == -1) {
            if (StrUtil.isEmpty(resources.getFailMsg())) {
                throw new BadRequestException("请填写失败原因");
            }
            String mark = "提现失败,退回佣金" + resources.getExtractPrice() + "元";
            PushMallUserDTO userDTO = pushMallUserService.findById(resources.getUid());

            //增加流水
            PushMallUserBill userBill = new PushMallUserBill();
            userBill.setTitle("提现失败");
            userBill.setUid(resources.getUid());
            userBill.setCategory("now_money");
            userBill.setType("extract");
            userBill.setNumber(resources.getExtractPrice());
            userBill.setLinkId(resources.getId().toString());
            userBill.setBalance(NumberUtil.add(userDTO.getBrokeragePrice(), resources.getExtractPrice()));
            userBill.setMark(mark);
            userBill.setStatus(1);
            userBill.setAddTime(OrderUtil.getSecondTimestampTwo());
            userBill.setPm(1);
            pushMallUserBillService.create(userBill);

            //返回提现金额
            pushMallUserService.incBrokeragePrice(resources.getExtractPrice().doubleValue()
                    , resources.getUid());

            resources.setFailTime(OrderUtil.getSecondTimestampTwo());

        }
        //todo 此处为企业付款，没经过测试
        boolean isTest = true;
        if (!isTest) {
            PushMallWechatUserDTO wechatUser = wechatUserService.findById(resources.getUid());
            if (ObjectUtil.isNotNull(wechatUser)) {
                try {
                    payService.entPay(wechatUser.getOpenid(), resources.getId().toString(),
                            resources.getRealName(),
                            resources.getExtractPrice().multiply(new BigDecimal(100)).intValue());
                } catch (WxPayException e) {
                    throw new BadRequestException(e.getMessage());
                }
            } else {
                throw new BadRequestException("不是微信用户无法退款");
            }

        }
        pushMallUserExtractService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
